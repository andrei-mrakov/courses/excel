const { resolve } = require( 'path' );

// plugins
const { CleanWebpackPlugin } = require( 'clean-webpack-plugin' );
// const CopyWebpackPlugin = require( 'copy-webpack-plugin' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );

// helpers
const isProd = process.env.NODE_ENV === 'production';
const isDev = !isProd;
const fileName = ext => isDev ? `bundle.${ ext }` : `bundle.[contenthash]${ ext }`;
const jsLoaders = () => {
    /**
     * @type { [ { loader: string, options: $ObjMap } | string]}
     */
    const loaders = [
        {
            loader: 'babel-loader',
            options: {
                presets: [ '@babel/preset-env' ],
                plugins: [ '@babel/plugin-proposal-class-properties' ],
            },
        },
    ];
    
    if( isDev ) {
        loaders.push( 'eslint-loader' );
    }
    
    return loaders;
};

module.exports = {
    context: resolve( __dirname, 'src' ),
    mode: 'development',
    entry: [ '@babel/polyfill', resolve( __dirname, 'src/index.js' ) ],
    devtool: isDev ? 'source-map' : false,
    // target: isDev && 'web',
    devServer: {
        contentBase: resolve( __dirname, 'build' ),
        historyApiFallback: true,
        overlay: {
            error: true,
        },
        compress: true,
        hot: true,
        port: 8080,
        open: 'firefox',
    },
    output: {
        filename: fileName( 'js' ),
        path: resolve( __dirname, 'build' ),
    },
    resolve: {
        extensions: [ '.js' ],
        alias: {
            '@': resolve( __dirname, 'src' ),
            '@core': resolve( __dirname, 'src/core' ),
        },
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin( {
            filename: 'index.html',
            template: resolve( __dirname, 'public/template.html' ),
            favicon: resolve( __dirname, 'public/favicon.ico' ),
            title: 'Pure JS Excel',
            minify: {
                removeComments: isProd,
                collapseWhitespace: isProd,
            },
        } ),
        new MiniCssExtractPlugin( {
            filename: fileName( 'css' ),
        } ),
    ],
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            // hmr: isDev,
                            // reloadAll: true,
                        },
                    },
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.js$/i,
                exclude: /node_module/,
                use: jsLoaders(),
            },
        ],
    },
};
