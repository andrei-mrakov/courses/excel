import { KEYBOARD_KEYS } from '@/components/table/TableSelection';

/**
 * @param { string } key
 * @return { boolean }
 */
export function shouldSubmit( key ) {
    return [ KEYBOARD_KEYS.enter, KEYBOARD_KEYS.tab ].includes( key );
}
