import { $ } from '@core/dom';
import { ExcelComponent } from '@core/ExcelComponent';
import { toEventName } from '@core/utils';
import { Table } from '@/components/table/Table';
import { shouldSubmit } from './formula.functions';

/**
 *
 */
export class Formula extends ExcelComponent {
    static className = 'excel__formula';
    name = Formula.name;
    
    /**
     * @param { Dom } $root
     * @param { ComponentOptions } options
     */
    constructor( $root, options ) {
        super( $root, {
            listeners: [ 'input', 'keydown' ],
            name,
            ...options,
        } );
    }
    
    /**
     * @return { void }
     */
    init() {
        super.init();
        this.$formula = $( '[data-type="formula"]' );
        
        // listeners
        this.$on( toEventName( Table, 'change' ), data => {
            this.$formula.text = data;
        } );
        this.$on( toEventName( Table, 'focus-cell' ), data => {
            this.$formula.text = data;
        } );
    }
    
    /**
     * @return { string }
     */
    toHTML() {
        return `
            <div class="formula__info">fx</div>
            <div
                class="formula__input"
                contenteditable=""
                spellcheck="false"
                data-type="formula"
            >
            </div>
        `;
    }
    
    /**
     * @param { InputEvent } event
     */
    onInput( event ) {
        this.$emit( toEventName( this, 'input' ), $( event.target ).text );
    }
    
    /**
     * @param { MouseEvent } event
     */
    onClick( event ) {
        console.log( 'event', event );
    }
    
    /**
     * @param { KeyboardEvent } event
     */
    onKeydown( event ) {
        if( shouldSubmit( event.key ) && !event.shiftKey ) {
            event.preventDefault();
            this.$emit( toEventName( Formula, 'submit' ) );
        }
    }
}
