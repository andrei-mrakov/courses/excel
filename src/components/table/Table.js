import { Formula } from '@/components/formula/Formula';
import { $ } from '@core/dom';
import { ExcelComponent } from '@core/ExcelComponent';
import { toEventName } from '@core/utils';
import { isCell, shouldMove } from './table.functions';
import { TableSelection } from './TableSelection';
import { shouldResize } from './table.functions';
import { resizeHandler } from './table.resize';
import { createTable } from './table.template';

/**
 *
 */
export class Table extends ExcelComponent {
    static className = 'excel__table';
    name = Table.name;
    
    /**
     * @param { Dom } $root
     * @param { ComponentOptions } options
     */
    constructor( $root, options ) {
        super( $root, {
            listeners: [ 'mousedown', 'keydown', 'input' ],
            name,
            ...options,
        } );
        /**
         * @type { TableSelection }
         */
        this.selection = null;
    }
    
    /**
     *
     */
    init() {
        super.init();
        const $cell = this.$root.find( '[data-id]' );
        this.selection = new TableSelection();
        this.selection.select( $cell );
        // copy init value from 1st cell to formula
        this.$emit(
            toEventName( Table, 'focus-cell' ),
            $cell.text,
        );
        // listeners
        this.$on( toEventName( Formula, 'input' ), data => {
            this.selection.current.text = data;
        } );
        this.$on( toEventName( Formula, 'submit' ), () => {
            this.selection.current.focus();
        } );
    }
    
    /**
     * @return { string }
     */
    toHTML() {
        return createTable( 10 );
    }
    
    /**
     * @param { object } event
     */
    onMousedown( event ) {
        const $target = $( event.target );
        if( shouldResize( event ) ) {
            resizeHandler( this.$root, $target );
        }
        
        if( isCell( event ) ) {
            if( event.shiftKey && this.selection.group.size ) {
                this.selection.selectGroup( this.$root, $target );
            } else {
                this.selection.select( $target, !event.ctrlKey );
                this.$emit(
                    toEventName( Table, 'focus-cell' ),
                    $target.text,
                );
            }
        }
    }
    
    /**
     * @param { KeyboardEvent } event
     */
    onKeydown( event ) {
        if( isCell( event ) ) {
            const { key } = event;
            if( shouldMove( key ) && !event.shiftKey ) {
                event.preventDefault();
                this.selection.move( this.$root, key );
                this.$emit(
                    toEventName( Table, 'focus-cell' ),
                    $(event.target).text,
                );
            }
        }
    }
    
    /**
     * @param { InputEvent } event
     */
    onInput( event ) {
        if( isCell( event ) ) {
            this.$emit(
                toEventName( Table, 'change' ),
                $( event.target ).text,
            );
        }
    }
}
