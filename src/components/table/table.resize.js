/**
 * @param { Dom } $table
 * @param { Dom } $resizer
 */
export function resizeHandler( $table, $resizer ) {
    const isColResizer = $resizer.data.resize === 'col'; // is resizing column
    const $resizerLine = $resizer.find( '[data-type="resizer-line"]' ); // resize line separator
    const $parent = $resizer.closest( '[data-type="resizable"]' ); // resizeable cell
    const coords = $parent.getCoords();
    const resizedCells = $table.findAll( `[data-col="${ $parent.data.col }"]` ); // resized cells
    let deltaValue;
    
    // change resizer line size
    if( isColResizer ) {
        $resizerLine.css( { height: $table.getCoords().height + 'px' } );
    } else {
        $resizerLine.css( { width: $table.getCoords().width + 'px' } );
    }
    
    document.onmousemove = e => {
        if( isColResizer ) {
            const delta = e.pageX - coords.right;
            deltaValue = coords.width + delta;
            // change resizer position
            $resizer.css( { right: -delta + 'px' } );
        } else {
            const delta = e.pageY - coords.bottom;
            deltaValue = coords.height + delta;
            // change resizer position
            $resizer.css( { bottom: -delta + 'px' } );
        }
    };
    
    document.onmouseup = () => {
        // remove listeners
        document.onmousemove = null;
        document.onmouseup = null;
        
        if( isColResizer ) {
            // change head cell width
            $parent.css( { width: deltaValue + 'px' } );
            // change resized cells width
            resizedCells.forEach( cell => {
                cell.css( { width: deltaValue + 'px' } );
            } );
            // reset resizer position
            $resizer.css( { right: '' } );
        } else {
            // change resized row height
            $parent.css( { height: deltaValue + 'px' } );
            // reset resizer position
            $resizer.css( { bottom: '' } );
        }
    };
}
