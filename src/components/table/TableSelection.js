import { nextCellSelector } from './table.functions';
import { range } from '@core/utils';

export const KEYBOARD_KEYS = {
    tab: 'Tab',
    left: 'ArrowLeft',
    up: 'ArrowUp',
    right: 'ArrowRight',
    down: 'ArrowDown',
    enter: 'Enter',
};

/**
 *
 */
export class TableSelection {
    static className = 'selected';
    
    /**
     *
     */
    constructor() {
        /**
         * @type { Set<Dom> }
         */
        this.group = new Set();
        /**
         * @type { Dom }
         */
        this.current = null;
    }
    
    /**
     * @param { Dom } $el
     * @param { boolean } withClearing
     */
    select( $el, withClearing = true ) {
        if( withClearing ) {
            this.clear();
        }
        this.current = $el;
        this.group.add( $el );
        $el.focus();
        $el.addClass( TableSelection.className );
    }
    
    /**
     * @param { Dom } $table
     * @param { string } key
     * @return { void }
     */
    move( $table, key ) {
        const { row, col } = this.current.id( true );
        const $nextEl = $table.find( nextCellSelector( key, { row, col } ) );
        if( $nextEl ) {
            this.select( $nextEl, true );
        }
    }
    
    /**
     * @param { Dom } $el
     * @return { void }
     */
    remove( $el ) {
        this.group.delete( $el );
    }
    
    /**
     * @param { Dom } $table
     * @param { Dom } $el
     */
    selectGroup( $table, $el ) {
        this.clear();
        const startCoords = this.current.id( true );
        const endCoords = $el.id( true );
        const rows = range( startCoords.row, endCoords.row );
        const cols = range( startCoords.col, endCoords.col );
        const cellsIDs = cols.reduce( ( acc, col ) => {
            return acc.concat( rows.map( row => `${ row }:${ col }` ) );
        }, [] );
        
        cellsIDs.forEach( id => {
            const $target = $table.find( `[data-id="${ id }"]` );
            $target.addClass( TableSelection.className );
            this.group.add( $target );
        } );
    }
    
    /**
     * @return { void }
     */
    clear() {
        this.group.forEach( $elem => $elem.removeClass( TableSelection.className ) );
        this.group.clear();
    }
}

