import { filledArray } from '@core/utils';

/**
 *
 * @typedef { 'col' | 'row' } ResizeType
 */

const CODES = {
    A: 65,
    Z: 90,
};

/**
 * @param { any } _
 * @param { number } idx
 // * @return { { id: number, val: string } }
 * @return { string }
 */
function fromCharCode( _, idx ) {
    // return {
    //     id: idx + 1,
    //     val: String.fromCharCode( CODES.A + idx ).toLocaleLowerCase(),
    // };
    return String.fromCharCode( CODES.A + idx ).toLocaleLowerCase();
}

/**
 * @param { ResizeType } type
 * @return { string }
 */
function createResizer( type ) {
    return `
        <div class="resizer" data-resize="${ type }">
            <div class="resizer-line" data-type="resizer-line"></div>
        </div>
    `;
}

/**
 * @param { string } rowName
 * @return { function }
 */
function createCell( rowName ) {
    return function( colName ) {
        return `
            <div
                class="cell"
                contenteditable=""
                spellcheck="false"
                data-col="${ colName }"
                data-id="${ rowName }:${ colName }"
            >1</div>
        `;
    };
}

/**
 * @param { any } colData
 * @param { number } idx
 * @return { string }
 */
function createHeadCell( colData, idx ) {
    return `
        <div
            class="column"
            data-type="resizable"
            data-col="${ idx + 1 }"
            >
            ${ fromCharCode( colData, idx ) }
            ${ createResizer( 'col' ) }
        </div>
    `;
}

/**
 * @param { string } [data = '']
 * @param { string } [info = '']
 * @return { string }
 */
function createRow( data = '', info = '' ) {
    return `
        <div class="row" data-type="resizable">
            <div class="row-info" >
                ${ info }
                ${ info && createResizer( 'row' ) }
            </div>
            <div class="row-data">
                ${ data }
            </div>
        </div>
    `;
}

/**
 * @param { number } colsCount
 * @return { string }
 */
function createHeadRow( colsCount ) {
    if( !colsCount > 0 ) {
        return '';
    } else {
        return createRow(
            filledArray( colsCount )
                .map( createHeadCell )
                .join( '' ),
        );
    }
}

/**
 * @param { number } colsCount
 * @param { number } rowsCount
 * @return { [ string ]  }
 */
function createDataRows( colsCount, rowsCount ) {
    if( !( colsCount > 0 && rowsCount > 0 ) ) {
        return [ '' ];
    } else {
        return filledArray( rowsCount )
            .map( ( _, idx ) => createRow(
                filledArray( colsCount )
                    .map( ( _, idx ) => idx + 1 )
                    .map( createCell( ( idx + 1 ).toString() ) )
                    .join( '' ),
                ( idx + 1 ).toString(),
            ) );
    }
}

/**
 * @param { number } [rowCount = 15]
 * @return { string }
 */
export function createTable( rowCount = 15 ) {
    const columnsCount = CODES.Z - CODES.A + 1; // +1 to include Z
    const headRow = createHeadRow( columnsCount );
    const dataRows = createDataRows( columnsCount, rowCount );
    
    return [ headRow, ...dataRows ].join( '' );
}
