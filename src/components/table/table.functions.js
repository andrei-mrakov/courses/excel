import { KEYBOARD_KEYS } from './TableSelection';

/**
 * @param { object } event
 * @return { boolean }
 */
export function shouldResize( event ) {
    return [ 'col', 'row' ].includes( event.target.dataset.resize );
}

/**
 * @param { object } event
 * @return { boolean }
 */
export function isCell( event ) {
    return !!event.target.dataset.id;
}

/**
 * @param { string } key
 * @return { boolean }
 */
export function shouldMove( key ) {
    return Object.values( KEYBOARD_KEYS ).includes( key );
}


/**
 * @param { string } key
 * @param { number } col
 * @param { number } row
 * @return { string }
 */
export function nextCellSelector( key, { col, row } ) {
    switch ( key ) {
        case KEYBOARD_KEYS.left:
            return `[data-id="${ row }:${ col - 1 }"]`;
        case KEYBOARD_KEYS.up:
            return `[data-id="${ row - 1 }:${ col }"]`;
        case KEYBOARD_KEYS.tab:
        case KEYBOARD_KEYS.right:
            return `[data-id="${ row }:${ col + 1 }"]`;
        case KEYBOARD_KEYS.enter:
        case KEYBOARD_KEYS.down:
            return `[data-id="${ row + 1 }:${ col }"]`;
        default:
            return '';
    }
}
