import { ExcelComponent } from '@core/ExcelComponent';

/**
 *
 */
export class Header extends ExcelComponent {
    static className = 'excel__header';
    name = Header.name;
    /**
     * @param { Dom } $root
     * @param { ComponentOptions } options
     */
    constructor( $root, options ) {
        super( $root, {
            name,
            ...options,
        } );
    }
    
    /**
     * @return { string }
     */
    toHTML() {
        return `
            <input class="header__input" type="text" value="Новая таблица" />
            <div class="header__buttons">
                <div class="button">
                    <i class="material-icons">delete</i>
                </div>
                <div class="button">
                    <i class="material-icons">exit_to_app</i>
                </div>
            </div>
        `;
    }
}
