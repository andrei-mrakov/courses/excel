import { $ } from '@core/dom';
import { Emitter } from '@core/Emitter';

/**
 *
 */
export class Excel {
    static className = 'excel';
    /**
     * @param { string } selector - DOM selector.
     * @param { object } options
     */
    constructor( selector, options ) {
        /**
         * @type { Element } $el - root element of excel app
         */
        this.$el = document.querySelector( selector );
        this.components = options.components || [];
        this.emitter = new Emitter();
    }
    
    /**
     * @return { HTMLElement }
     */
    getRoot() {
        const $root = $.create( 'div', Excel.className );
        const options = {
            emitter: this.emitter,
        };
        
        this.components = this.components.map( Component => {
            // создаём обёртку для каждого компонента
            const $el = $.create( 'div', Component.className );
            
            // создаём каждый компонент
            const component = new Component( $el, options );
            $el.html = component.toHTML();
            $root.append( $el );
            
            return component;
        } );
        
        return $root.$el;
    }
    
    /**
     * @return { void }
     */
    render() {
        this.$el.append( this.getRoot() );
        this.components.forEach( component => component.init() );
        // this.components.forEach( component => component.destroy() );
    }
    /**
     * @return { void }
     */
    destroy() {
        this.components.forEach( component => component.destroy() );
    }
}
