import { capitalize } from '@core/utils';

/**
 *
 */
export class DomListener {
    /**
     * @param { Dom } $root
     * @param { [string] } listeners
     */
    constructor( $root, listeners = [] ) {
        if( !$root ) {
            throw new Error( 'No root provided.' );
        }
        this.$root = $root;
        this.listeners = listeners.map( event => ( { event, listener: null } ) );
    }
    
    /**
     * @return { void }
     */
    initDOMListeners() {
        this.listeners.forEach( listenerObj => {
            const method = getMethodName( listenerObj.event );
            if( !this[method] ) {
                throw new Error(
                    'Method ' + method + 'is not implemented on ' +
                    this.constructor.name + ' component',
                );
            }
            listenerObj.listener = this[method].bind( this );
            this.$root.on( listenerObj.event, listenerObj.listener );
        } );
    }
    
    /**
     * @return { void }
     */
    removeDOMListeners() {
        this.listeners.forEach( listenerObj => {
            this.$root.off( listenerObj.event, listenerObj.listener );
        } );
    }
}

/**
 * @param { string } eventName
 * @return { string }
 */
function getMethodName( eventName ) {
    return `on${ capitalize( eventName ) }`;
}
