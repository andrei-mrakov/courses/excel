/**
 * jquery analogue
 */
class Dom {
    /**
     * @param { string| HTMLElement } selector - selector = string or DOM node
     */
    constructor( selector ) {
        this.$el = typeof selector === 'string'
            ? document.querySelector( selector )
            : selector;
    }
    
    /**
     * @param { string } [html]
     */
    set html( html ) {
        this.$el.innerHTML = html;
    }
    
    /**
     * @return { string }
     */
    get html() {
        return this.$el.outerHTML.trim();
    }
    
    /**
     */
    get text() {
        if( this.$el.tagName.toLowerCase() === 'input' ) {
            return this.$el.value.trim();
        }
        
        return this.$el.textContent.trim();
    }
    
    /**
     * @param { string } text
     */
    set text( text ) {
        this.$el.textContent = text;
    }
    
    /**
     * @return { Dom }
     */
    clear() {
        this.html = '';
        
        return this;
    }
    
    /**
     * @param { HTMLElement | Dom } node
     * @return { Dom } node
     */
    append( node ) {
        if( node instanceof Dom ) {
            this.$el.append( node.$el );
        } else {
            this.$el.append( node );
        }
        
        return this;
    }
    
    /**
     * @param { string } selector
     * @return { Dom }
     */
    closest( selector ) {
        return $( this.$el.closest( selector ) );
    }
    
    /**
     * @param { string } selector
     * @return { Dom }
     */
    find( selector ) {
        const founded = this.$el.querySelector( selector );
        if( !founded ) {
            return null;
        } else {
            return $( founded );
        }
    }
    
    /**
     * @param { string } selector
     * @return { [Dom] | [] }
     */
    findAll( selector ) {
        const $nodes = this.$el.querySelectorAll( selector );
        if( $nodes.length ) {
            return [ ...$nodes ].map( $ );
        }
        
        return [];
    }
    
    /**
     * @param { string } className
     * @return { Dom }
     */
    addClass( className ) {
        this.$el.classList.add( className );
        
        return this;
    }
    
    /**
     * @param { string } className
     * @return { Dom }
     */
    removeClass( className ) {
        this.$el.classList.remove( className );
        
        return this;
    }
    
    /**
     * @return { DOMStringMap }
     */
    get data() {
        return this.$el.dataset;
    }
    
    /**
     * @param { DOMStringMap } field
     * @return { void }
     */
    set data( field ) {
        this.$el.dataset[field] = field;
    }
    
    /**
     * @param { boolean } parsed
     * @return { string | { row: number, col: number } }
     */
    id( parsed = false ) {
        if( !parsed ) {
            return this.id();
        }
        const [ row, col ] = this.data.id.split( ':' ).map( Number );
        
        return {
            row, col,
        };
    }
    
    /**
     * @param { object } [styles = {}]
     * @return { void }
     */
    css( styles = {} ) {
        Object.keys( styles ).forEach( key => {
            this.$el.style[key] = styles[key];
        } );
    }
    
    /**
     * @return { DOMRect }
     */
    getCoords() {
        return this.$el.getBoundingClientRect();
    }
    
    /**
     * @param { $ObjMap } options
     * @return { Dom }
     */
    focus( options = {} ) {
        this.$el.focus( options );
        
        return this;
    }
    
    /**
     * @param { string } eventName
     * @param { EventListener } callback
     * @return { Dom }
     */
    on( eventName, callback ) {
        this.$el.addEventListener( eventName, callback );
        
        return this;
    }
    
    /**
     * @param { string } eventName
     * @param { EventListener } callback
     * @return { Dom }
     */
    off( eventName, callback ) {
        this.$el.removeEventListener( eventName, callback );

        return this;
    }
}

/**
 * @param { string|Element|EventTarget } selector
 * @return { Dom }
 */
export function $( selector ) {
    return new Dom( selector );
}

/**
 * @param { string } tagName
 * @param { string } [classNames = '']
 * @return { Dom }
 */
$.create = ( tagName, classNames = '' ) => {
    const $el = document.createElement( tagName );
    if( classNames ) {
        $el.classList.add( classNames );
    }
    
    return $( $el );
};
