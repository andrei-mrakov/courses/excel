import { DomListener } from '@core/DomListener';

/**
 *
 */
export class ExcelComponent extends DomListener {
    /**
     * @typedef { { listeners: [string], emitter: Emitter, name: string } } ComponentOptions
     */
    /**
     * @param { Dom } $root
     * @param { ComponentOptions } options
     */
    constructor( $root, options = {} ) {
        super( $root, options.listeners );
        this.name = options.name || '';
        this.emitter = options.emitter;
        this.unsubscrivers = [];
    }
    
    /**
     * @throws { Error }
     */
    toHTML() {
        throw new Error( 'You must override this method.' );
    }
    
    /**
     * @param { string } eventName
     * @param { any } args
     */
    $emit( eventName, ...args ) {
        this.emitter.emit( eventName, ...args );
    }
    
    /**
     * @param { string } eventName
     * @param { function } fn
     */
    $on( eventName, fn ) {
        const unsub = this.emitter.subscribe( eventName, fn );
        this.unsubscrivers.push( unsub );
    }
    
    /**
     * @return { void }
     */
    init() {
        this.initDOMListeners();
    }
    
    /**
     * @return { void }
     */
    destroy() {
        this.removeDOMListeners();
        this.unsubscrivers.forEach( unsub => unsub() );
    }
}
