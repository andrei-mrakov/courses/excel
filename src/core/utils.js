/**
 * @param { string } str
 * @return { string }
 */
export function capitalize( str ) {
    if( !str ) {
        return '';
    }
    
    return str.charAt( 0 ).toUpperCase().concat( str.slice( 1 ) );
}

/**
 * @param { number } arrNum
 * @param { any } [filler = '']
 * @return { [string] }
 */
export function filledArray( arrNum, filler = '' ) {
    return new Array( arrNum ).fill( filler );
}

/**
 * @param { number } start
 * @param { number } end
 * @return { [ number ] }
 */
export function range( start, end ) {
    const [ min, max ] = start > end ? [ end, start ] : [ start, end ];
    
    return new Array( max - min + 1 ).fill( '' ).map( ( _, idx ) => min + idx );
}

/**
 * @param { object } component
 * @param { string } eventName
 * @return { string }
 */
export function toEventName( component, eventName) {
    return `[${ component.name }]:${ eventName }`;
}
