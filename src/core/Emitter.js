/**
 *
 */
export class Emitter {
    /**
     *
     */
    constructor() {
        this.listeners = {};
    }
    
    /**
     * @param { string } eventName
     * @param { any } args
     * @return { boolean }
     */
    emit( eventName, ...args ) {
        if( !Array.isArray( this.listeners[eventName] ) ) {
            return false;
        }
        
        this.listeners[eventName].forEach( listener => {
            listener( ...args );
        } );
        
        return true;
    }
    
    /**
     * @param { string }  eventName
     * @param { function } func
     * @return { function } func
     */
    subscribe( eventName, func ) {
        this.listeners[eventName] = this.listeners[eventName] || [];
        this.listeners[eventName].push( func );
        
        return () => {
            this.listeners[eventName] = this.listeners[eventName].filter( listener => listener !== func );
        };
    }
}
